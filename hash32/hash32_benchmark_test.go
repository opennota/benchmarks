package hash32_test

import (
	"testing"

	xxhash "github.com/OneOfOne/xxhash/native"
	farm "github.com/dgryski/go-farm"
	spooky "github.com/dgryski/go-spooky"
	crc32clmul "github.com/klauspost/crc32"
	"github.com/opennota/fasthash"
	"github.com/zhenjl/cityhash"
	"hash/adler32"
	"hash/crc32"
	"hash/fnv"
)

var buf = make([]byte, 8192)

var hcrcieee = func(p []byte) uint32 { return crc32.ChecksumIEEE(p) }

func BenchmarkCRCIEEE8(b *testing.B)   { benchmarkHash8(b, hcrcieee) }
func BenchmarkCRCIEEE16(b *testing.B)  { benchmarkHash16(b, hcrcieee) }
func BenchmarkCRCIEEE40(b *testing.B)  { benchmarkHash40(b, hcrcieee) }
func BenchmarkCRCIEEE64(b *testing.B)  { benchmarkHash64(b, hcrcieee) }
func BenchmarkCRCIEEE128(b *testing.B) { benchmarkHash128(b, hcrcieee) }
func BenchmarkCRCIEEE1K(b *testing.B)  { benchmarkHash1K(b, hcrcieee) }
func BenchmarkCRCIEEE8K(b *testing.B)  { benchmarkHash8K(b, hcrcieee) }

var hcrcieecl = func(p []byte) uint32 { return crc32clmul.ChecksumIEEE(p) }

func BenchmarkCRCIEEECL8(b *testing.B)   { benchmarkHash8(b, hcrcieecl) }
func BenchmarkCRCIEEECL16(b *testing.B)  { benchmarkHash16(b, hcrcieecl) }
func BenchmarkCRCIEEECL40(b *testing.B)  { benchmarkHash40(b, hcrcieecl) }
func BenchmarkCRCIEEECL64(b *testing.B)  { benchmarkHash64(b, hcrcieecl) }
func BenchmarkCRCIEEECL128(b *testing.B) { benchmarkHash128(b, hcrcieecl) }
func BenchmarkCRCIEEECL1K(b *testing.B)  { benchmarkHash1K(b, hcrcieecl) }
func BenchmarkCRCIEEECL8K(b *testing.B)  { benchmarkHash8K(b, hcrcieecl) }

var castagnoliTable = crc32.MakeTable(crc32.Castagnoli)
var hcrccastagnoli = func(p []byte) uint32 { return crc32.Checksum(p, castagnoliTable) }

func BenchmarkCRCCastagnoli8(b *testing.B)   { benchmarkHash8(b, hcrccastagnoli) }
func BenchmarkCRCCastagnoli16(b *testing.B)  { benchmarkHash16(b, hcrccastagnoli) }
func BenchmarkCRCCastagnoli40(b *testing.B)  { benchmarkHash40(b, hcrccastagnoli) }
func BenchmarkCRCCastagnoli64(b *testing.B)  { benchmarkHash64(b, hcrccastagnoli) }
func BenchmarkCRCCastagnoli128(b *testing.B) { benchmarkHash128(b, hcrccastagnoli) }
func BenchmarkCRCCastagnoli1K(b *testing.B)  { benchmarkHash1K(b, hcrccastagnoli) }
func BenchmarkCRCCastagnoli8K(b *testing.B)  { benchmarkHash8K(b, hcrccastagnoli) }

var koopmanTable = crc32.MakeTable(crc32.Koopman)
var hcrckoopman = func(p []byte) uint32 { return crc32.Checksum(p, koopmanTable) }

func BenchmarkCRCKoopman8(b *testing.B)   { benchmarkHash8(b, hcrckoopman) }
func BenchmarkCRCKoopman16(b *testing.B)  { benchmarkHash16(b, hcrckoopman) }
func BenchmarkCRCKoopman40(b *testing.B)  { benchmarkHash40(b, hcrckoopman) }
func BenchmarkCRCKoopman64(b *testing.B)  { benchmarkHash64(b, hcrckoopman) }
func BenchmarkCRCKoopman128(b *testing.B) { benchmarkHash128(b, hcrckoopman) }
func BenchmarkCRCKoopman1K(b *testing.B)  { benchmarkHash1K(b, hcrckoopman) }
func BenchmarkCRCKoopman8K(b *testing.B)  { benchmarkHash8K(b, hcrckoopman) }

var hadler32 = func(p []byte) uint32 { return adler32.Checksum(p) }

func BenchmarkAdler8(b *testing.B)   { benchmarkHash8(b, hadler32) }
func BenchmarkAdler16(b *testing.B)  { benchmarkHash16(b, hadler32) }
func BenchmarkAdler40(b *testing.B)  { benchmarkHash40(b, hadler32) }
func BenchmarkAdler64(b *testing.B)  { benchmarkHash64(b, hadler32) }
func BenchmarkAdler128(b *testing.B) { benchmarkHash128(b, hadler32) }
func BenchmarkAdler1K(b *testing.B)  { benchmarkHash1K(b, hadler32) }
func BenchmarkAdler8K(b *testing.B)  { benchmarkHash8K(b, hadler32) }

var hfnv = func(p []byte) uint32 { h := fnv.New32(); h.Write(p); return h.Sum32() }

func BenchmarkFnv8(b *testing.B)   { benchmarkHash8(b, hfnv) }
func BenchmarkFnv16(b *testing.B)  { benchmarkHash16(b, hfnv) }
func BenchmarkFnv40(b *testing.B)  { benchmarkHash40(b, hfnv) }
func BenchmarkFnv64(b *testing.B)  { benchmarkHash64(b, hfnv) }
func BenchmarkFnv128(b *testing.B) { benchmarkHash128(b, hfnv) }
func BenchmarkFnv1K(b *testing.B)  { benchmarkHash1K(b, hfnv) }
func BenchmarkFnv8K(b *testing.B)  { benchmarkHash8K(b, hfnv) }

var hfnv32a = func(p []byte) uint32 { h := fnv.New32a(); h.Write(p); return h.Sum32() }

func BenchmarkFnv32a8(b *testing.B)   { benchmarkHash8(b, hfnv32a) }
func BenchmarkFnv32a16(b *testing.B)  { benchmarkHash16(b, hfnv32a) }
func BenchmarkFnv32a40(b *testing.B)  { benchmarkHash40(b, hfnv32a) }
func BenchmarkFnv32a64(b *testing.B)  { benchmarkHash64(b, hfnv32a) }
func BenchmarkFnv32a128(b *testing.B) { benchmarkHash128(b, hfnv32a) }
func BenchmarkFnv32a1K(b *testing.B)  { benchmarkHash1K(b, hfnv32a) }
func BenchmarkFnv32a8K(b *testing.B)  { benchmarkHash8K(b, hfnv32a) }

var hfasthash = func(p []byte) uint32 { return fasthash.Hash32(0, p) }

func BenchmarkFasthash8(b *testing.B)   { benchmarkHash8(b, hfasthash) }
func BenchmarkFasthash16(b *testing.B)  { benchmarkHash16(b, hfasthash) }
func BenchmarkFasthash40(b *testing.B)  { benchmarkHash40(b, hfasthash) }
func BenchmarkFasthash64(b *testing.B)  { benchmarkHash64(b, hfasthash) }
func BenchmarkFasthash128(b *testing.B) { benchmarkHash128(b, hfasthash) }
func BenchmarkFasthash1K(b *testing.B)  { benchmarkHash1K(b, hfasthash) }
func BenchmarkFasthash8K(b *testing.B)  { benchmarkHash8K(b, hfasthash) }

var hspooky = func(p []byte) uint32 { return spooky.Hash32(p) }

func BenchmarkSpooky8(b *testing.B)   { benchmarkHash8(b, hspooky) }
func BenchmarkSpooky16(b *testing.B)  { benchmarkHash16(b, hspooky) }
func BenchmarkSpooky40(b *testing.B)  { benchmarkHash40(b, hspooky) }
func BenchmarkSpooky64(b *testing.B)  { benchmarkHash64(b, hspooky) }
func BenchmarkSpooky128(b *testing.B) { benchmarkHash128(b, hspooky) }
func BenchmarkSpooky1K(b *testing.B)  { benchmarkHash1K(b, hspooky) }
func BenchmarkSpooky8K(b *testing.B)  { benchmarkHash8K(b, hspooky) }

var hxxhash = func(p []byte) uint32 { return xxhash.Checksum32(p) }

func BenchmarkXXHash8(b *testing.B)   { benchmarkHash8(b, hxxhash) }
func BenchmarkXXHash16(b *testing.B)  { benchmarkHash16(b, hxxhash) }
func BenchmarkXXHash40(b *testing.B)  { benchmarkHash40(b, hxxhash) }
func BenchmarkXXHash64(b *testing.B)  { benchmarkHash64(b, hxxhash) }
func BenchmarkXXHash128(b *testing.B) { benchmarkHash128(b, hxxhash) }
func BenchmarkXXHash1K(b *testing.B)  { benchmarkHash1K(b, hxxhash) }
func BenchmarkXXHash8K(b *testing.B)  { benchmarkHash8K(b, hxxhash) }

var hcity = func(p []byte) uint32 { return cityhash.CityHash32(p, uint32(len(p))) }

func BenchmarkCity8(b *testing.B)   { benchmarkHash8(b, hcity) }
func BenchmarkCity16(b *testing.B)  { benchmarkHash16(b, hcity) }
func BenchmarkCity40(b *testing.B)  { benchmarkHash40(b, hcity) }
func BenchmarkCity64(b *testing.B)  { benchmarkHash64(b, hcity) }
func BenchmarkCity128(b *testing.B) { benchmarkHash128(b, hcity) }
func BenchmarkCity1K(b *testing.B)  { benchmarkHash1K(b, hcity) }
func BenchmarkCity8K(b *testing.B)  { benchmarkHash8K(b, hcity) }

var hfarm = func(p []byte) uint32 { return farm.Hash32(p) }

func BenchmarkFarm8(b *testing.B)   { benchmarkHash8(b, hfarm) }
func BenchmarkFarm16(b *testing.B)  { benchmarkHash16(b, hfarm) }
func BenchmarkFarm40(b *testing.B)  { benchmarkHash40(b, hfarm) }
func BenchmarkFarm64(b *testing.B)  { benchmarkHash64(b, hfarm) }
func BenchmarkFarm128(b *testing.B) { benchmarkHash128(b, hfarm) }
func BenchmarkFarm1K(b *testing.B)  { benchmarkHash1K(b, hfarm) }
func BenchmarkFarm8K(b *testing.B)  { benchmarkHash8K(b, hfarm) }

func benchmarkHash8(b *testing.B, h func([]byte) uint32) {
	b.SetBytes(8)
	for i := 0; i < b.N; i++ {
		h(buf[:8])
	}
}

func benchmarkHash16(b *testing.B, h func([]byte) uint32) {
	b.SetBytes(16)
	for i := 0; i < b.N; i++ {
		h(buf[:16])
	}
}

func benchmarkHash40(b *testing.B, h func([]byte) uint32) {
	b.SetBytes(40)
	for i := 0; i < b.N; i++ {
		h(buf[:40])
	}
}

func benchmarkHash64(b *testing.B, h func([]byte) uint32) {
	b.SetBytes(64)
	for i := 0; i < b.N; i++ {
		h(buf[:64])
	}
}

func benchmarkHash128(b *testing.B, h func([]byte) uint32) {
	b.SetBytes(128)
	for i := 0; i < b.N; i++ {
		h(buf[:128])
	}
}

func benchmarkHash1K(b *testing.B, h func([]byte) uint32) {
	b.SetBytes(1024)
	for i := 0; i < b.N; i++ {
		h(buf[:1024])
	}
}

func benchmarkHash8K(b *testing.B, h func([]byte) uint32) {
	b.SetBytes(8192)
	for i := 0; i < b.N; i++ {
		h(buf[:8192])
	}
}
