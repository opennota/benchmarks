package hash64_test

import (
	"testing"

	xxhash "github.com/OneOfOne/xxhash/native"
	"github.com/dchest/siphash"
	farm "github.com/dgryski/go-farm"
	metro "github.com/dgryski/go-metro"
	spooky "github.com/dgryski/go-spooky"
	"github.com/opennota/fasthash"
	"github.com/zhenjl/cityhash"
	"hash/crc64"
	"hash/fnv"
)

var buf = make([]byte, 8192)

var ecmaTable = crc64.MakeTable(crc64.ECMA)
var hcrcecma = func(p []byte) uint64 { return crc64.Checksum(p, ecmaTable) }

func BenchmarkCRCECMA8(b *testing.B)   { benchmarkHash8(b, hcrcecma) }
func BenchmarkCRCECMA16(b *testing.B)  { benchmarkHash16(b, hcrcecma) }
func BenchmarkCRCECMA40(b *testing.B)  { benchmarkHash40(b, hcrcecma) }
func BenchmarkCRCECMA64(b *testing.B)  { benchmarkHash64(b, hcrcecma) }
func BenchmarkCRCECMA128(b *testing.B) { benchmarkHash128(b, hcrcecma) }
func BenchmarkCRCECMA1K(b *testing.B)  { benchmarkHash1K(b, hcrcecma) }
func BenchmarkCRCECMA8K(b *testing.B)  { benchmarkHash8K(b, hcrcecma) }

var isoTable = crc64.MakeTable(crc64.ISO)
var hcrciso = func(p []byte) uint64 { return crc64.Checksum(p, isoTable) }

func BenchmarkCRCISO8(b *testing.B)   { benchmarkHash8(b, hcrciso) }
func BenchmarkCRCISO16(b *testing.B)  { benchmarkHash16(b, hcrciso) }
func BenchmarkCRCISO40(b *testing.B)  { benchmarkHash40(b, hcrciso) }
func BenchmarkCRCISO64(b *testing.B)  { benchmarkHash64(b, hcrciso) }
func BenchmarkCRCISO128(b *testing.B) { benchmarkHash128(b, hcrciso) }
func BenchmarkCRCISO1K(b *testing.B)  { benchmarkHash1K(b, hcrciso) }
func BenchmarkCRCISO8K(b *testing.B)  { benchmarkHash8K(b, hcrciso) }

var hfnv = func(p []byte) uint64 { h := fnv.New64(); h.Write(p); return h.Sum64() }

func BenchmarkFnv8(b *testing.B)   { benchmarkHash8(b, hfnv) }
func BenchmarkFnv16(b *testing.B)  { benchmarkHash16(b, hfnv) }
func BenchmarkFnv40(b *testing.B)  { benchmarkHash40(b, hfnv) }
func BenchmarkFnv64(b *testing.B)  { benchmarkHash64(b, hfnv) }
func BenchmarkFnv128(b *testing.B) { benchmarkHash128(b, hfnv) }
func BenchmarkFnv1K(b *testing.B)  { benchmarkHash1K(b, hfnv) }
func BenchmarkFnv8K(b *testing.B)  { benchmarkHash8K(b, hfnv) }

var hfnv64a = func(p []byte) uint64 { h := fnv.New64a(); h.Write(p); return h.Sum64() }

func BenchmarkFnv64a8(b *testing.B)   { benchmarkHash8(b, hfnv64a) }
func BenchmarkFnv64a16(b *testing.B)  { benchmarkHash16(b, hfnv64a) }
func BenchmarkFnv64a40(b *testing.B)  { benchmarkHash40(b, hfnv64a) }
func BenchmarkFnv64a64(b *testing.B)  { benchmarkHash64(b, hfnv64a) }
func BenchmarkFnv64a128(b *testing.B) { benchmarkHash128(b, hfnv64a) }
func BenchmarkFnv64a1K(b *testing.B)  { benchmarkHash1K(b, hfnv64a) }
func BenchmarkFnv64a8K(b *testing.B)  { benchmarkHash8K(b, hfnv64a) }

var hfasthash = func(p []byte) uint64 { return fasthash.Hash64(0, p) }

func BenchmarkFasthash8(b *testing.B)   { benchmarkHash8(b, hfasthash) }
func BenchmarkFasthash16(b *testing.B)  { benchmarkHash16(b, hfasthash) }
func BenchmarkFasthash40(b *testing.B)  { benchmarkHash40(b, hfasthash) }
func BenchmarkFasthash64(b *testing.B)  { benchmarkHash64(b, hfasthash) }
func BenchmarkFasthash128(b *testing.B) { benchmarkHash128(b, hfasthash) }
func BenchmarkFasthash1K(b *testing.B)  { benchmarkHash1K(b, hfasthash) }
func BenchmarkFasthash8K(b *testing.B)  { benchmarkHash8K(b, hfasthash) }

var hspooky = func(p []byte) uint64 { return spooky.Hash64(p) }

func BenchmarkSpooky8(b *testing.B)   { benchmarkHash8(b, hspooky) }
func BenchmarkSpooky16(b *testing.B)  { benchmarkHash16(b, hspooky) }
func BenchmarkSpooky40(b *testing.B)  { benchmarkHash40(b, hspooky) }
func BenchmarkSpooky64(b *testing.B)  { benchmarkHash64(b, hspooky) }
func BenchmarkSpooky128(b *testing.B) { benchmarkHash128(b, hspooky) }
func BenchmarkSpooky1K(b *testing.B)  { benchmarkHash1K(b, hspooky) }
func BenchmarkSpooky8K(b *testing.B)  { benchmarkHash8K(b, hspooky) }

var hxxhash = func(p []byte) uint64 { return xxhash.Checksum64(p) }

func BenchmarkXXHash8(b *testing.B)   { benchmarkHash8(b, hxxhash) }
func BenchmarkXXHash16(b *testing.B)  { benchmarkHash16(b, hxxhash) }
func BenchmarkXXHash40(b *testing.B)  { benchmarkHash40(b, hxxhash) }
func BenchmarkXXHash64(b *testing.B)  { benchmarkHash64(b, hxxhash) }
func BenchmarkXXHash128(b *testing.B) { benchmarkHash128(b, hxxhash) }
func BenchmarkXXHash1K(b *testing.B)  { benchmarkHash1K(b, hxxhash) }
func BenchmarkXXHash8K(b *testing.B)  { benchmarkHash8K(b, hxxhash) }

var hcity = func(p []byte) uint64 { return cityhash.CityHash64(p, uint32(len(p))) }

func BenchmarkCity8(b *testing.B)   { benchmarkHash8(b, hcity) }
func BenchmarkCity16(b *testing.B)  { benchmarkHash16(b, hcity) }
func BenchmarkCity40(b *testing.B)  { benchmarkHash40(b, hcity) }
func BenchmarkCity64(b *testing.B)  { benchmarkHash64(b, hcity) }
func BenchmarkCity128(b *testing.B) { benchmarkHash128(b, hcity) }
func BenchmarkCity1K(b *testing.B)  { benchmarkHash1K(b, hcity) }
func BenchmarkCity8K(b *testing.B)  { benchmarkHash8K(b, hcity) }

var hmetro = func(p []byte) uint64 { return metro.Hash64_1(p, 0) }

func BenchmarkMetro8(b *testing.B)   { benchmarkHash8(b, hmetro) }
func BenchmarkMetro16(b *testing.B)  { benchmarkHash16(b, hmetro) }
func BenchmarkMetro40(b *testing.B)  { benchmarkHash40(b, hmetro) }
func BenchmarkMetro64(b *testing.B)  { benchmarkHash64(b, hmetro) }
func BenchmarkMetro128(b *testing.B) { benchmarkHash128(b, hmetro) }
func BenchmarkMetro1K(b *testing.B)  { benchmarkHash1K(b, hmetro) }
func BenchmarkMetro8K(b *testing.B)  { benchmarkHash8K(b, hmetro) }

var hsiphash = func(p []byte) uint64 { return siphash.Hash(0, 0, p) }

func BenchmarkSiphash8(b *testing.B)   { benchmarkHash8(b, hsiphash) }
func BenchmarkSiphash16(b *testing.B)  { benchmarkHash16(b, hsiphash) }
func BenchmarkSiphash40(b *testing.B)  { benchmarkHash40(b, hsiphash) }
func BenchmarkSiphash64(b *testing.B)  { benchmarkHash64(b, hsiphash) }
func BenchmarkSiphash128(b *testing.B) { benchmarkHash128(b, hsiphash) }
func BenchmarkSiphash1K(b *testing.B)  { benchmarkHash1K(b, hsiphash) }
func BenchmarkSiphash8K(b *testing.B)  { benchmarkHash8K(b, hsiphash) }

var hfarm = func(p []byte) uint64 { return farm.Hash64(p) }

func BenchmarkFarm8(b *testing.B)   { benchmarkHash8(b, hfarm) }
func BenchmarkFarm16(b *testing.B)  { benchmarkHash16(b, hfarm) }
func BenchmarkFarm40(b *testing.B)  { benchmarkHash40(b, hfarm) }
func BenchmarkFarm64(b *testing.B)  { benchmarkHash64(b, hfarm) }
func BenchmarkFarm128(b *testing.B) { benchmarkHash128(b, hfarm) }
func BenchmarkFarm1K(b *testing.B)  { benchmarkHash1K(b, hfarm) }
func BenchmarkFarm8K(b *testing.B)  { benchmarkHash8K(b, hfarm) }

func benchmarkHash8(b *testing.B, h func([]byte) uint64) {
	b.SetBytes(8)
	for i := 0; i < b.N; i++ {
		h(buf[:8])
	}
}

func benchmarkHash16(b *testing.B, h func([]byte) uint64) {
	b.SetBytes(16)
	for i := 0; i < b.N; i++ {
		h(buf[:16])
	}
}

func benchmarkHash40(b *testing.B, h func([]byte) uint64) {
	b.SetBytes(40)
	for i := 0; i < b.N; i++ {
		h(buf[:40])
	}
}

func benchmarkHash64(b *testing.B, h func([]byte) uint64) {
	b.SetBytes(64)
	for i := 0; i < b.N; i++ {
		h(buf[:64])
	}
}

func benchmarkHash128(b *testing.B, h func([]byte) uint64) {
	b.SetBytes(128)
	for i := 0; i < b.N; i++ {
		h(buf[:128])
	}
}

func benchmarkHash1K(b *testing.B, h func([]byte) uint64) {
	b.SetBytes(1024)
	for i := 0; i < b.N; i++ {
		h(buf[:1024])
	}
}

func benchmarkHash8K(b *testing.B, h func([]byte) uint64) {
	b.SetBytes(8192)
	for i := 0; i < b.N; i++ {
		h(buf[:8192])
	}
}
